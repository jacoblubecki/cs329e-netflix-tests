#!/usr/bin/env python3

# -------
# imports
# -------

import time

from unittest import main, TestCase, expectedFailure
from math import sqrt
from io import StringIO
from os import path

from numpy import sqrt, square, mean, subtract
from requests import get

from Netflix import netflix_eval
from Util import get_probe, generate_inputs

# ------------
# Test Helpers
# ------------

PROBE_LEN = 1425333

# -----------
# TestNetflix
# -----------


class TestNetflix(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Reading and evaluating the probe data is expensive.
        # Both are done here to speed up testing.

        # Read in the probe data
        cls.probe_data = get_probe()

        # Evaluate the probe data
        r = StringIO(cls.probe_data)
        w = StringIO()

        start = time.perf_counter()
        netflix_eval(r, w)

        cls.eval_time = time.perf_counter() - start
        cls.probe_result = w.getvalue()

    # ---------
    # gen input
    # ---------

    def test_gen_inputs_correct(self):
        # Test a single valid input
        r = StringIO('10040:\n2417853\n1207062\n2487973\n')
        input_gen = generate_inputs(r)
        movie, users = next(input_gen)
        self.assertEqual(movie, 10040)
        self.assertEqual(users, [2417853, 1207062, 2487973])

        # Test multiple valid inputs
        r = StringIO('10040:\n2417853\n10040:\n1207062\n2487973\n')
        input_gen = generate_inputs(r)

        movie, users = next(input_gen)
        self.assertEqual(movie, 10040)
        self.assertEqual(users, [2417853])

        movie, users = next(input_gen)
        self.assertEqual(movie, 10040)
        self.assertEqual(users, [1207062, 2487973])

    def test_gen_inputs_partially_correct(self):
        # Movie with no users
        r = StringIO('1:')
        input_gen = generate_inputs(r)

        movie, users = next(input_gen)
        self.assertEqual(movie, 1)
        self.assertEqual(users, [])

        # Movie with some users and a movie with no users
        r = StringIO('1:\n2:\n1\n2\n3')
        input_gen = generate_inputs(r)

        movie, users = next(input_gen)
        self.assertEqual(movie, 1)
        self.assertEqual(users, [])

        movie, users = next(input_gen)
        self.assertEqual(movie, 2)
        self.assertEqual(users, [1, 2, 3])

        # Same as above, but with junk data added
        r = StringIO('1\n2\n3\n1:\n2:\n1\n2\n3')
        input_gen = generate_inputs(r)

        movie, users = next(input_gen)
        self.assertEqual(movie, 1)
        self.assertEqual(users, [])

        movie, users = next(input_gen)
        self.assertEqual(movie, 2)
        self.assertEqual(users, [1, 2, 3])

    def test_gen_inputs_no_movie(self):
        r = StringIO('12324\n4324221\n154243\n124643\n')
        input_gen = generate_inputs(r)
        with self.assertRaises(StopIteration):
            next(input_gen)

        r = StringIO('')
        input_gen = generate_inputs(r)
        with self.assertRaises(StopIteration):
            next(input_gen)

    # ----
    # eval
    # ----

    def test_eval_bad_movie(self):
        r = StringIO('-1:\n2417853\n1207062\n2487973\n')
        w = StringIO()
        with self.assertRaises(KeyError):
            netflix_eval(r, w)

    def test_eval_basic(self):
        r = StringIO('10040:\n2417853\n1207062\n2487973\n')
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10040:\n4.0\n4.0\n4.0\n1.15\n')

    def test_eval_probe_len(self):
        # The probe has 1425333 records per project specifications
        input_lines = self.probe_data.splitlines()
        probe_records = list(filter(str.strip, input_lines))
        self.assertEqual(len(probe_records), PROBE_LEN)

        # Should have one extra output containing the RMSE
        output_lines = self.probe_result.splitlines()
        output_records = list(filter(str.strip, output_lines))
        self.assertEqual(len(output_records), PROBE_LEN + 1)

    def test_eval_probe_result(self):
        """Tests the speed and accuracy of the evaluation function.

        Since probe.txt contains all possible inputs, this is effectively a
        corner case for the netflix_eval function.

        Note:
            This should be marked as an expected failure until evaluation
            speed and accuracy both meet required specifications.
        """
        # Gets all the non-empty lines of the output
        output_lines = self.probe_result.splitlines()
        output_records = list(filter(str.strip, output_lines))

        # Should have RMSE less than 1
        error = float(output_records[-1])
        self.assertLess(error, 1)

        # Should take less than 60 seconds to evaluate probe.txt
        self.assertLess(self.eval_time, 60)


# ----
# main
# ----

if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
