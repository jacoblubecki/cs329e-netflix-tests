#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_read, netflix_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------


class TestNetflix (TestCase):

    # ------------
    # netflix_read
    # ------------
   
    def test_read_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        self.assertEqual(netflix_read(r), {'10040':['2417853','1207062','2487973']})

    def test_read_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n10:\n1952305\n1531863\n")
        self.assertEqual(netflix_read(r), {'10040':['2417853','1207062','2487973'], '10':['1952305', '1531863']})

    def test_read_3(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n10:\n1952305\n1531863\n10000:\n200206\n523108\n")
        self.assertEqual(netflix_read(r), {'10040':['2417853','1207062','2487973'], '10':['1952305', '1531863'], '10000':['200206', '523108']})

    # ------------
    # netflix_prediction
    # ------------
   
    def test_prediction_1(self):        
        self.assertEqual(netflix_prediction('10',[1952305, 1531863]),[3.3,3.2])
    

    def test_prediction_2(self):        
        self.assertEqual(netflix_prediction('1',[30878, 2647871, 1283744, 2488120, 317050, 1904905, 1989766]),[3.6,3.5,3.6,4.2,3.6,3.7,3.5])
    
    def test_prediction_3(self):        
        self.assertEqual(netflix_prediction('10024',[2059382,1210053,1654205,2027932,311377,2161379,1611053,516686,2529292,2248946]),[3.5, 3.9, 3.5, 3.9, 3.5, 3.4, 3.4, 3.4, 3.6, 3.6])



    # ----
    # eval
    # ----

    def test_eval_1(self):
        read1 = open("./probe.txt",'r')
        re = ""
        for line in read1:
            re += line
        read1.close()
        r = StringIO(re)
        w = StringIO()
        self.assertLessEqual(netflix_eval(r, w), 1)
        r.close()
        
   
    def test_eval_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        self.assertLessEqual(netflix_eval(r, w), 1)
        
    
    def test_eval_3(self):
        read1 = open("./smallprobe1.txt",'r')
        re = ""
        for line in read1:
            re += line
        read1.close()
        r = StringIO(re)
        w = StringIO()
        self.assertLessEqual(netflix_eval(r, w), 1)
        r.close()

    def test_eval_4(self):
        read1 = open("./smallprobe2.txt",'r')
        re = ""
        for line in read1:
            re += line
        read1.close()
        r = StringIO(re)        
        w = StringIO()
        self.assertEqual(netflix_eval(r, w), 1.05)
        r.close()

    
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
