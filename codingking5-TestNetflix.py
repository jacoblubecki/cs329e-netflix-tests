#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, parse_movie_id, parse_customer_id, calc_rmse
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3365\n3.214\n3.5835\n0.83\n")

    # tests if the customer_ids are being parsed correctly
    def test_netflix_parses_cust_id(self):
        r_one = "1:\n30878\n2647871\n"
        ci = parse_customer_id(r_one)
        self.assertEqual(ci,'30878')

        r_two = ("10:\n1952305\n")
        ci2 = parse_customer_id(r_two)
        self.assertEqual(ci2,'1952305')

        r_three = ("1000:\n977808\n")
        ci3 = parse_customer_id(r_three)
        self.assertEqual(ci3,'977808')

        r_four = ("10000:\n200206\n")
        ci4 = parse_customer_id(r_four)
        self.assertEqual(ci4,'200206')

        r_five = ("10001:\n262828\n")
        ci5 = parse_customer_id(r_five)
        self.assertEqual(ci5, '262828')

    # tests if the movie_ids are being parsed correctly
    def test_netflix_parses_movie_id(self):
        r_one = ("1:\n30878\n")
        mi1 = parse_movie_id(r_one)
        self.assertEqual(mi1, '1')

        r_two = ("10:\n1952305\n")
        mi2 = parse_movie_id(r_two)
        self.assertEqual(mi2 ,'10')

        r_three = ("1000:\n977808\n")
        mi3 = parse_movie_id(r_three)
        self.assertEqual(mi3 ,'1000')

        r_four = ("10000:\n200206\n")
        mi4 = parse_movie_id(r_four)
        self.assertEqual(mi4 ,'10000')

        r_five = ("10001:\n262828\n")
        mi5 = parse_movie_id(r_five)
        self.assertEqual(mi5, '10001')

    # tests that the RMSE calculated is as expected
    def valid_rmse(self):
        # PREDICTIONS = [3.55, 4.1, 2.65, 3.15, 2.75]
        # RMSE_cal = 1.45
        test_actual_ratings = [3, 3, 5, 2.5, 1]
        test_avg_movie_rating = [3.6, 4.7, 3.3, 2.3, 1]
        test_avg_yearly_customer_rating = [3.5, 3.5, 2, 4, 4.5]
        test_pred_ratings = []
        for i in range(len(test_actual_ratings)):
            test_pred_ratings.append((test_avg_movie_rating[i]+test_avg_yearly_customer_rating[i])/2)

        calc_rmse(test_pred_ratings, test_actual_ratings)
        self.assertEqual("1.45")


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
