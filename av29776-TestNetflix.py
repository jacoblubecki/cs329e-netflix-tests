#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, no_outlier_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

class TestNetflix (TestCase):

	# ----
	# eval
	# ----

	def test_eval_1(self):
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "3445:\n3.0\n3.1\n3.3\n3.2\n0.76\n")

	def test_eval_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "15581:\n3.4\n0.6\n")

	def test_eval_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "16330:\n3.0\n3.3\n3.2\n3.2\n3.7\n0.96\n")

	def test_movie_1(self):
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "3445:")

	def test_movie_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "15581:")

	def test_movie_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "16330:")

	def test_cust_1(self):
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.0","3.1","3.3","3.2"])

	def test_cust_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.4"])

	def test_cust_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.0","3.3","3.2","3.2","3.7"])

	def test_rmse_1(self):
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertLess(float(x[-1]), 1)

	def test_rmse_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertLess(float(x[-1]), 1)

	def test_rmse_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertLess(float(x[-1]), 1)

	def test_cache_1(self):
		r = {1:5, 2:12, 3:4, 4:3, 5:0, 6:6, 7:4}
		s = no_outlier_cache(r)
		self.assertEqual(s, {1:5, 3:4, 4:3, 5:0, 6:6, 7:4})

	def test_cache_2(self):
		r = {1:19, 2:16, 3:5, 4:7, 10:8, 6:13, 7:12, 8:0, 9: 22, 5:-45, 11:69}
		s = no_outlier_cache(r)
		self.assertEqual(s, {1:19, 2:16, 3:5, 4:7, 10:8, 6:13, 7:12, 8:0, 9: 22})

	def test_cache_3(self):
		r = {1:1, 2:1, 3:7, 4:7, 5:7, 6:7, 7:9, 8:7, 9:10, 10:7, 11:7, 12:7}
		s = no_outlier_cache(r)
		self.assertEqual(s, {3:7, 4:7, 5:7, 6:7, 8:7, 10:7, 11:7, 12:7})
# ----
# main
# ----			
if __name__ == '__main__':
	main()



# -----------
# TestNetflix
# -----------

'''class TestNetflix (TestCase):

	# ----
	# eval
	# ----

	def test_eval_1(self): #tests reading properly
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "3445:\n3.0\n3.1\n3.3\n3.2\n0.76\n")

	def test_eval_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "15581:\n3.4\n0.6\n")

	def test_eval_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(w.getvalue(), "16330:\n3.1\n3.3\n3.2\n3.2\n3.6\n1.01\n")

	def test_movie_1(self):
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "3445:")

	def test_movie_2(self): #tests movie outputs
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "15581:")

	def test_movie_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[0], "16330:")

	def test_cust_1(self): #tests prediction value outputs
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.0","3.1","3.3","3.2"])

	def test_cust_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.4"])

	def test_cust_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(x[1:len(x)-1], ["3.1","3.3","3.2","3.2","3.6"])

	def test_rmse_1(self): #tests rmse values (hand calculated)
		r = StringIO("3445:\n703893\n2131425\n218536\n1884606\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(float(x[-1]), 0.76)

	def test_rmse_2(self):
		r = StringIO("15581:\n1786736\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(float(x[-1]), 0.6)

	def test_rmse_3(self):
		r = StringIO("16330:\n611900\n1813897\n272497\n1520336\n2223771\n")
		w = StringIO()
		netflix_eval(r, w)
		x = w.getvalue()
		x = x.split()
		self.assertEqual(float(x[-1]), 1.01)

	def test_cache_1(self): #gets rid of outliers, tests new dictionary
		r = {1:5, 2:12, 3:4, 4:3, 5:0, 6:6, 7:4}
		s = no_outlier_cache(r)
		self.assertEqual(s, {1:5, 3:4, 4:3, 5:0, 6:6, 7:4})

	def test_cache_2(self):
		r = {1:19, 2:16, 3:5, 4:7, 10:8, 6:13, 7:12, 8:0, 9: 22, 5:-45, 11:69}
		s = no_outlier_cache(r)
		self.assertEqual(s, {1:19, 2:16, 3:5, 4:7, 10:8, 6:13, 7:12, 8:0, 9: 22})

	def test_cache_3(self):
		r = {1:1, 2:1, 3:7, 4:7, 5:7, 6:7, 7:9, 8:7, 9:10, 10:7, 11:7, 12:7}
		s = no_outlier_cache(r)
		self.assertEqual(s, {3:7, 4:7, 5:7, 6:7, 8:7, 10:7, 11:7, 12:7})
# ----
# main
# ----			
if __name__ == '__main__':
	main()
'''

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out

16330:
3.0
3.3
3.2
3.2
3.7

16330:
611900
1813897
272497
1520336
2223771

% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
