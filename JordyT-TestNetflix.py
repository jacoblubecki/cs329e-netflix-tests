#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
from requests import get


# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    # test_eval-prediction_values ensures that the predictions are being done correctly
    def test_eval_prediction_values(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7\n3.5\n3.5\n4.2\n3.7\n3.8\n0.67\n")

    # test_output_movie_number makes sure that the movie number is being printed correctly
    def test_output_movie_number(self):
        r = StringIO("1000:\n2326571\n977808\n1010534\n1861759\n79755")
        w = StringIO()
        netflix_eval(r, w)
        l = w.getvalue()
        self.assertEqual(l[0:4], "1000")

    # test_rsme_less_one predicts a larger sample of reviews and ensures that the rsme is less than one
    def test_rmse_less_one(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n10:\n1952305\n1531863\n1000:\n2326571\n977808\n1010534\n1861759\n79755\n98259\n1960212\n97460\n2623506\n2409123\n1959111\n809597\n2251189\n537705\n929584\n506737\n708895\n1900790\n2553920\n1196779\n2411446\n1002296\n1580442\n100291\n433455\n2368043\n906984\n10000:\n200206\n523108\n10001:\n262828\n2609496\n1474804\n831991\n267142\n2305771\n220050\n1959883\n27822\n2570808\n90355\n2417258\n264764\n143866\n766895\n714089\n2350428\n10002:\n1450941\n1213181\n308502\n2581993\n10003:\n1515111\n10004:\n1737087\n1270334\n1262711\n1903515\n2140798\n2479158\n2161335\n2099522\n1291600\n2198877\n2214717\n1936582\n84804\n1257912\n1263375\n735147\n1186407\n350806\n1141242\n638738\n590598\n1704823\n2238464\n273497\n2499539\n2543444\n119309\n2540335\n129495\n956464\n1648424\n10005:\n254775\n1892654\n469365\n793736\n926698\n10006:\n1093333\n1982605\n1534853\n1632583\n10007:\n1204847\n2160216\n248206\n835054\n1064667\n2419805\n2084848\n671106\n2087887\n1340891\n1917538\n2018945\n2520477\n10008:\n1813636\n2048630\n930946\n1492860\n1687570\n1122917\n1885441\n10009:\n1927533\n1789102\n2263612\n964421\n701514\n2120902\n1001:\n1050889\n67976\n1025642\n624334\n239718\n549109\n143504\n584301\n437680\n2434971\n400819\n1841130\n1926199\n805709\n545109\n662875\n74408\n529725\n1786012\n2382523\n1358422\n780049\n1894639\n1014799\n2176030\n286045\n1381947\n386254\n224172\n404085\n2005305\n1956315\n2580275\n2110738\n1525405\n2187027\n1995906\n2133433\n2097983\n884805\n2083077\n1642134")
        w = StringIO()
        netflix_eval(r, w)
        l = w.getvalue()
        self.assertLess(float(l[-5:-1]), 1)


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
