#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.4\n3.3\n3.7\n0.84\n")

    def test_eval_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10:\n3.3\n3.2\n0.25\n")

    def test_eval_3(self):
        r = StringIO("10008:\n1813636\n2048630\n930946\n1492860\n1687570\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10008:\n4.3\n3.4\n3.6\n3.2\n4.0\n0.88\n")

# ----
# main
# ----			
if __name__ == '__main__':
    main()